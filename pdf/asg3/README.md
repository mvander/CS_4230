Code is located in CS_4230/src/tbb/asg3/. There are two folders. The first folder holds the code for problems 1 and 2 and the second for problems 3-5 (they are labeled as such).
To run, simply use the make file with the correct settings as specified by Vinu on the main class Gitlab.

## Files:

#### Problem1-2:

DP_tbb.cpp -> basic dot product in TBB.

DP_tbb_test.cpp -> timing test code for TBB dot product.

OMP_DP_test.cpp -> timing test code modeled after above for comparisons.

#### Problem3-5:

merge_sort.cpp -> merge sort using McCool implementation. 

.h files are also McCool mergesort implementations.

quicksort_tbb.cpp -> quick sort using McCool implementation.

Note!!! Both of the above cpp files will provide output that is limited to the thread count followed by the timing results. This was to make it easier to port results into Latex.