/* File:     asg5vecadd.c
 * DERIVED FROM Pacheco's mpi_vector_add.c
 *
 * Purpose:  Implement parallel vector addition using a block
 *           distribution of the vectors.  This version also
 *           illustrates the use of MPI_Scatter, MPI_Gather,
 *           and MPI_Reduce.
 *
 * Compile:  mpicc -g -Wall -o mpi_vector_add mpi_vector_add.c
 * Run:      mpiexec -n <comm_sz> ./vector_add
 *
 * Input:    The order of the vectors, n
 * Output:   The sum vector z = x+y
 *
 * Notes:     
 * 1.  The order of the vectors, n, should be evenly divisible
 *     by comm_sz
 *
 * 2.  The original program mpi_vector_add.c does a lot of error-checking
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

/* File:     timer.h
 *
 * Purpose:  Define a macro that returns the number of seconds that 
 *           have elapsed since some point in the past.  The timer
 *           should return times with microsecond accuracy.
 *
 * Note:     The argument passed to the GET_TIME macro should be
 *           a double, *not* a pointer to a double.
 *
 * Example:  
 *    #include "timer.h"
 *    . . .
 *    double start, finish, elapsed;
 *    . . .
 *    GET_TIME(start);
 *    . . .
 *    Code to be timed
 *    . . .
 *    GET_TIME(finish);
 *    elapsed = finish - start;
 *    printf("The code to be timed took %e seconds\n", elapsed);
 *
 * IPP:  Section 3.6.1 (pp. 121 and ff.) and Section 6.1.2 (pp. 273 and ff.)
 */

#ifndef _TIMER_H_
#define _TIMER_H_

#include <sys/time.h>

/* The argument now should be a double (not a pointer to a double) */
#define GET_TIME(now) { \
  struct timeval t; \
  gettimeofday(&t, NULL); \
  now = t.tv_sec + t.tv_usec/1000000.0; \
  }

#endif


/*-------------------------------------------------------------------
 * Function:  Parallel_vector_sum
 * Purpose:   Add a vector that's been distributed among the processes
 * In args:   local_x:  local storage of one of the vectors being added
 *            local_y:  local storage for the second vector being added
 *            local_n:  the number of components in local_x, local_y,
 *                      and local_z
 * Out arg:   local_z:  local storage for the sum of the two vectors
 */
void Parallel_vector_sum(
			 double  local_x[]  /* in  */, 
			 double  local_y[]  /* in  */, 
			 double  local_z[]  /* out */, 
			 int     local_n    /* in  */) {
  int local_i;

  for (local_i = 0; local_i < local_n; local_i++)
    local_z[local_i] = local_x[local_i] + local_y[local_i];
}  /* Parallel_vector_sum */


/*-------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
  int n, local_n;
  int comm_sz, my_rank;

  // This is an n-sized buffer that's allocated within rank 0
  // for inputting x and y, and outputting z
  //
  double* full_buff = NULL;    
  double *local_x, *local_y, *local_z = NULL;

  // To tell which fn is erring
  char *fname = "Main";

  // To check status of mallocs mainly
  int local_ok = 1;

  // To spot failures in any of the ranks. "ok" is computed via reduction
  int ok = 1;

  // Communicator in which actions are happening
  MPI_Comm comm;

  // Variables to obtain timings
  double start, finish, elapsed;   

  MPI_Init(&argc, &argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_size(comm, &comm_sz);
  MPI_Comm_rank(comm, &my_rank);

  // Get size of problem
  if (my_rank == 0) {
    printf("What's the order of the vectors?\n");
    scanf("%d", &n);
  }

  // Let everyone know this "n", whether it be good or not!
  // This is because if this 'n' is bad, we need to have
  // each process shut itself down anyhow.
  MPI_Bcast(&n, 1, MPI_INT, 0, comm);

  if (n <= 0 || n % comm_sz != 0) {
    // Rank 0 alone reports the error
    if (my_rank == 0) {
      fprintf(stderr, "Bad problem size given\n");
      fflush(stderr);
    }
    // All ranks finalize and exit
    MPI_Finalize();
    exit(-1);
  }

  // Else all ranks go to work
  local_n = n/comm_sz;   


  // This malloc is happening in all the ranks
  local_x = malloc(local_n*sizeof(double));
  local_y = malloc(local_n*sizeof(double));
  local_z = malloc(local_n*sizeof(double));

  // Each rank is computing its "local_ok"
  if (local_x == NULL || local_y == NULL || 
      local_z == NULL) local_ok = 0;

  // Now see if any rank had an issue with any of
  // its mallocs; thus MPI_MIN will return 0 if 
  // any one allocation failed
  MPI_Allreduce(&local_ok, &ok, 1, MPI_INT, MPI_MIN, comm);
   
  if (ok == 0) {
    if (my_rank == 0) {
      fprintf(stderr, "A malloc indeed failed\n");
      fflush(stderr);
    }
    // All ranks finalize and exit
    MPI_Finalize();
    exit(-1);
  }
     
  // Generate the x-vector within rank-0 and scatter to other procs

  if (my_rank == 0) {
    full_buff = malloc(n*sizeof(double));   
    if (full_buff == NULL) {
      fprintf(stderr, "A malloc indeed failed\n");
      fflush(stderr);
      local_ok = 0;
    }
  }

  MPI_Allreduce(&local_ok, &ok, 1, MPI_INT, MPI_MIN, comm);

  if (ok == 0) {
    MPI_Finalize();
    exit(-1);
  }   

  if (my_rank == 0) {
    for (int i = 0; i < n; i++)
      full_buff[i] = (double) i;
  }

  MPI_Scatter(full_buff, local_n, MPI_DOUBLE, local_x, local_n, MPI_DOUBLE, 0,
	      comm);

  if (my_rank == 0) {
    for (int i = 0; i < n; i++)
      full_buff[i] = (double) (n-i-1);

    printf("n = %d, local_n = %d\n", n, local_n);
  }
   
  MPI_Scatter(full_buff, local_n, MPI_DOUBLE, local_y, local_n, MPI_DOUBLE, 0,
	      comm);

   
  GET_TIME(start);
   
  // Perform the localized summation in each proc
  // This is being performed by all ranks including rank 0
  for (int i=0; i < 512; i++)
    Parallel_vector_sum(local_x, local_y, local_z, local_n);

  // This puts the sum back into full_buff
  MPI_Gather(local_z, local_n, MPI_DOUBLE, full_buff, local_n, MPI_DOUBLE,
	     0, comm);

  GET_TIME(finish);

  elapsed = finish - start;
      
  if (my_rank == 0) {
    for (int i = 0; i < n; i++) {
      if (i < 32) {
	printf("%f ", full_buff[i]); // print a sampling of the values
      }
    }
    printf("\nElapsed time = %f\n", elapsed);
    free(full_buff);
  }   

  free(local_x);
  free(local_y);
  free(local_z);

  MPI_Finalize();

  return 0;
} 
// end of main
