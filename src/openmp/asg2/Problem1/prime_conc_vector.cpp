#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <omp.h>
#include "tbb/tbb.h"

using namespace std;

int main(int argc, char* argv[]) {
  
  // Grab out the program arguments and parse.
  if (argc < 3) {
    cout << "Plz provide # threads and prime range" << endl;
    exit(EXIT_FAILURE);
  }
  int thread_count = strtol(argv[1], NULL, 10);
  int primes_range = strtol(argv[2], NULL, 10);
  cout << "thread_count and primes_range are " << thread_count
       << ", " << primes_range << endl;
  
  // Create our concurrent vector where we will store all our prime values.
  tbb::concurrent_vector<int> prime_numbers;

  // Start the parallel section of code (removed reduction step as we can now safely just add
  // to our vector).
  #pragma omp parallel num_threads(thread_count)
  {
    #pragma omp for schedule(dynamic, 100)
    for(int i = 3; i <= primes_range; i += 2)
      {
	// Algorithm copied from example.
	int limit, prime;
	limit = (int) sqrt((float)i) + 1;
	prime = 1; // Assume the number is a prime.
	int j = 3;

	while (prime && (j <= limit))
	  {
	    if (i%j == 0) prime = 0;
	    j += 2;
	  }
	if (prime) 
	  {
	    prime_numbers.push_back(i);
	  }
      } 
  }

  // Now if the number of primes is under 256, print them out, otherwise print out the number of primes found.
  if (prime_numbers.size() <= 256) {
    cout << "Primes are:" << endl;
    for (int num: prime_numbers) {
      cout << num << endl;
    }
  } else {
    cout << "Found " << prime_numbers.size() << " primes." << endl;
  }
}
