#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <omp.h>

using namespace std;

// Global arrays where to store the values.
double *A;
double *B;

int main(int argc, char* argv[]) {
  
  // Grab out the program arguments and parse.
  if (argc < 4) {
    cout << "Usage:\n./OMP_DP <probsize> <threshold> <numthreads>" << endl;
    exit(EXIT_FAILURE);
  }
  int prob_size = strtol(argv[1], NULL, 10);
  int threshold = strtol(argv[2], NULL, 10);
  int thread_count = strtol(argv[3], NULL, 10);  

  // Create starting values.
  A = new double[prob_size];
  B = new double[prob_size];
  cout << "Filling arrays with starting values." << endl;
  srand(17); // Seed with 17.
  for (int i=0; i < prob_size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }

  // Variable for final product.
  double result = 0.0;

  cout << "Starting parallel dot product" << endl;

  #pragma omp parallel num_threads(thread_count)
  {
    #pragma omp for schedule(static, threshold) reduction(+: result)
    for (int index = 0; index < prob_size; ++index) {
      double local_result = A[index] * B[index];
      result += local_result;
    }
  }

  cout << "Result is: " << result << endl;

  // Free up memory used.
  delete[] A;
  delete[] B;
}
