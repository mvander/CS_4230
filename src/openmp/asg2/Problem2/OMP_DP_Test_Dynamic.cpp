/*
OMP_DP_Test_Dynamic.cpp

Slightly modified version of OMP_DP w/ Dynamic Scheduling to run tests quickly and output
timing results to a CSV file.
 */
#include <iostream>
#include <fstream>
#include <cmath>
#include <stdlib.h>
#include <omp.h>
#include <timer.h>

using namespace std;

// Global arrays where to store the values.
double *A;
double *B;

int REPEAT_TEST = 10;

int main(int argc, char* argv[]) {
  
  // Grab out the program arguments and parse.
  if (argc < 3) {
    cout << "Usage:\n./OMP_DP_Test_Dynamic.exe <probsize> <numthreads>" << endl;
    exit(EXIT_FAILURE);
  }
  int prob_size = strtol(argv[1], NULL, 10);
  int thread_count = strtol(argv[2], NULL, 10);  

  // Open file where we can print out results.
  ofstream results_csv;
  results_csv.open("results.csv");

  // Create starting values.
  A = new double[prob_size];
  B = new double[prob_size];
  cout << "Filling arrays with starting values." << endl;
  srand(17); // Seed with 17.
  for (int i=0; i < prob_size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }

  results_csv << "Threshold";
  for (int threads = 1; threads <= thread_count; ++threads) {
    results_csv << ",Thread " << threads;
  }
  results_csv << "\n";

  for (int threshold = 2; threshold <= prob_size; threshold = threshold * 2) {
    for (int threads = 1; threads <= thread_count; ++threads) {

      double start;
      GET_TIME(start);

      for (int test = 0; test < REPEAT_TEST; ++test) {
	// Variable for final product.
	double result = 0.0;

        #pragma omp parallel num_threads(threads)
	{
          #pragma omp for schedule(dynamic, threshold) reduction(+: result)
          for (int index = 0; index < prob_size; ++index) {
	    double local_result = A[index] * B[index];
	    result += local_result;
	  } 
	}
      }

      double finish;
      GET_TIME(finish);

      double elapsed = finish - start;

      cout << "Finished dot prod: used " << threads << " threads w/ threshold of " << threshold << endl;

      if (threads == thread_count) {
	results_csv << "," << elapsed << "\n";
      } else if (threads == 1) {
	results_csv << threshold << "," << elapsed;
      } else {
	results_csv << "," << elapsed;
      }
    }
  }

  delete[] A;
  delete[] B;
   

  results_csv.close();
}
