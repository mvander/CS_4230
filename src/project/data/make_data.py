# make_data.py
#
# Generate testing data for GPU vs. CPU speed tests.

import random
import numpy as np

def gen_setup_txt(M, V, prefix):
    
    pi = []
    for _ in range(M):
        pi.append(random.random())
    pi /= np.sum(pi)

    print(np.sum(pi))

    A = []
    for _ in range(M):
        a = []
        for _ in range(M):
            a.append(random.random())
        A.append(a)
    a_sums = np.sum(A, axis=1)

    for i in range(M):
        A[i] /= a_sums[i]

    B = []
    for _ in range(M):
        b = []
        for _ in range(V):
            b.append(random.random())
        B.append(b)
    b_sums = np.sum(B, axis=1)

    for i in range(M):
        B[i] /= b_sums[i]

    f = open(prefix + "_" + str(M) + "_" + str(V) + ".txt", "w")

    f.write(str(M) + "," + str(V) + "\n")

    pi_l = [str(elem) for elem in pi]
    pi_str = ",".join(pi_l)
    f.write(str(pi_str) + "\n")

    a_l = [str(elem) for a in A for elem in a]
    a_str = ",".join(a_l)
    f.write(str(a_str) + "\n")

    b_l = [str(elem) for b in B for elem in b]
    b_str = ",".join(b_l)
    f.write(str(b_str) + "\n")

    f.close()

def gen_data_txt(N, V, L, prefix):
    
    X = []

    for _ in range(N):
        x = []
        for _ in range(L):
            x.append(random.randrange(V))
        X.append(x)

    # Write data to file.
    f = open(prefix + "_" + str(N) + "_" + str(V) + "_" + str(L) + ".txt", "w")

    f.write(str(N) + "," + str(L) + "\n")
    
    x_l = [str(elem) for x in X for elem in x]
    x_str = ",".join(x_l)
    f.write(str(x_str) + "\n")
        
gen_setup_txt(50, 50, "l_tests/setup")

i = 10
while i < 200:
    gen_data_txt(50,50,i, "l_tests/data")
    i = i + 10
