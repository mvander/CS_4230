#include <stdio.h>
//#include <sys/sysinfo.h>
#include <pthread.h> 
#include <stdlib.h>
#include <math.h>
#include <timer.h>

const long MAX_EXP = 32;
int Exp, Thres;

typedef struct {
  int L; // Low bound.
  int H; // High bound.
} RNG;

// Global arrays over which DP is done
float *A;
float *B;
// Result array.
float *C;

void Usage(char *prog_name) {
   fprintf(stderr, "usage: %s <Exp>:int <Thres>:int\n", prog_name);
   fprintf(stderr, "Ensure that Thres <= pow(2, Exp)\n");
   exit(0);
}  /* Usage */

void Get_args(int argc, char **argv) {
   if (argc != 3) Usage(argv[0]);
   Exp = strtol(argv[1], NULL, 10);  
   if (Exp <= 0 || Exp > MAX_EXP) Usage(argv[0]);
   Thres = strtol(argv[2], NULL, 10);
   if (Thres < 1 || Thres >= (int) pow(2, Exp)) Usage(argv[0]);
}  

void serdp(RNG rng) {
  float local_sum = 0;

  // Multiply corresponding parts of the arrays.
  // Simultaneously add them to the sum here to better use locality.
  for(int i=rng.L; i<=rng.H; ++i) {
    C[i] = A[i] * B[i];
    local_sum += C[i];
  }
  
  // Set left-most in range to the local sum.
  C[rng.L] = local_sum;
}

void *pdp(void *rng) {
  RNG myrng = *((RNG *) rng);

  if ((myrng.H - myrng.L) <= Thres) {
    // This will multiply the corresponding items in this range, then add up the results, placing 
    // the result in the left most item.
    serdp(myrng);
  }
  else {
    //printf("-> myrng.L and myrng.H are %d %d\n", myrng.L, myrng.H);
    
    // Split the range.
    RNG rngL = myrng;
    RNG rngH = myrng;
    
    rngL.H = myrng.L + (myrng.H - myrng.L)/2;
    rngH.L = rngL.H+1;

    // Create and run the new threads.
    //printf("--> creating thread for range %d %d\n", rngL.L, rngL.H);
    pthread_t lower_thread_id;
    pthread_create(&lower_thread_id, NULL, pdp, (void *) &rngL);

    //printf("--> creating thread for range %d %d\n", rngH.L, rngH.H);
    pthread_t higher_thread_id;
    pthread_create(&higher_thread_id, NULL, pdp, (void *) &rngH);

    // Join the threads.
    pthread_join(lower_thread_id, NULL);
    pthread_join(higher_thread_id, NULL);

    // Now we know the lower of each of our thread ranges holds the values we want to reduce farther.
    C[myrng.L] = C[rngL.L] + C[rngH.L];
  }

  return NULL;
}

int get_nprocs_conf(void);
int get_nprocs(void);

int main(int argc, char **argv) {
  // Turn this on on Linux systems
  // On Mac, it does not work
  printf("This system has\
          %d processors configured and\
  	      %d processors available.\n",
          get_nprocs_conf(), get_nprocs());
  
  Get_args(argc, argv);  
  int Size = (int) pow(2, Exp);
  printf("Will do DP of %d sized arrays\n", Size);

  // Set up our two arrays to operate on.
  A = (float *) malloc(Size*sizeof(float));
  B = (float *) malloc(Size*sizeof(float));
  printf("Filling arrays now\n");
  srand(17); //Seed with 17
  for (int i=0; i<Size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }
  //  for(int i=0; i<Size; ++i) {
  //printf("A[%d] and B[%d] are %f and %f\n",i,i,(double)A[i],(double)B[i]);
  //  }

  // Create the first range that is all inclusive.
  RNG rng;
  rng.L = 0;
  rng.H = Size-1;
  
  // Set up our result array.
  C = (float *) malloc(Size*sizeof(float));
  for(int i=0; i<Size; ++i) {
    C[i]=0;
  }

  printf("Now invoking parallel dot product\n");

  // Perform a parallel dot product.
  double start;
  GET_TIME(start);
  
  pdp(&rng);

  double finish;
  GET_TIME(finish);

  double elapsed = finish - start;
  printf("The elapsed time is %e seconds\n", elapsed);

  //printf("Final C is\n");
  //for(int i=0; i<Size; ++i) {
  //printf("%f\n", (double) C[i]);
  //}

  printf("Final dot product result is %f\n",(double) C[0]);
  printf("Num of threads: %f\n", (double) Size / Thres);

  free(A);
  free(B);
  free(C);
}
