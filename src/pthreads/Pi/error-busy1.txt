bash-4.2$ gcc-7.3 -o pth_pi_busy1.exe -fsanitize=thread -static-libtsan pth_pi_busy1.c
bash-4.2$ ./pth_pi_busy1.exe 4  1000000
==================
WARNING: ThreadSanitizer: data race (pid=21564)
  Read of size 4 at 0x0000014998e0 by thread T2:
    #0 Thread_sum <null> (pth_pi_busy1.exe+0x00000048c952)

  Previous write of size 4 at 0x0000014998e0 by thread T1:
    #0 Thread_sum <null> (pth_pi_busy1.exe+0x00000048c9ef)

  Location is global 'flag' of size 4 at 0x0000014998e0 (pth_pi_busy1.exe+0x0000014998e0)

  Thread T2 (tid=21567, running) created by main thread at:
    #0 pthread_create ../../.././libsanitizer/tsan/tsan_interceptors.cc:900 (pth_pi_busy1.exe+0x00000040c850)
    #1 main <null> (pth_pi_busy1.exe+0x00000048c580)

  Thread T1 (tid=21566, running) created by main thread at:
    #0 pthread_create ../../.././libsanitizer/tsan/tsan_interceptors.cc:900 (pth_pi_busy1.exe+0x00000040c850)
    #1 main <null> (pth_pi_busy1.exe+0x00000048c580)

SUMMARY: ThreadSanitizer: data race (/home/u0920663/CS_4230/src/pthreads/Pi/pth_pi_busy1.exe+0x48c952) in Thread_sum
==================
==================
WARNING: ThreadSanitizer: data race (pid=21564)
  Read of size 8 at 0x0000014998d8 by thread T2:
    #0 Thread_sum <null> (pth_pi_busy1.exe+0x00000048c988)

  Previous write of size 8 at 0x0000014998d8 by thread T1:
    #0 Thread_sum <null> (pth_pi_busy1.exe+0x00000048c9a4)

  Location is global 'sum' of size 8 at 0x0000014998d8 (pth_pi_busy1.exe+0x0000014998d8)

  Thread T2 (tid=21567, running) created by main thread at:
    #0 pthread_create ../../.././libsanitizer/tsan/tsan_interceptors.cc:900 (pth_pi_busy1.exe+0x00000040c850)
    #1 main <null> (pth_pi_busy1.exe+0x00000048c580)

  Thread T1 (tid=21566, running) created by main thread at:
    #0 pthread_create ../../.././libsanitizer/tsan/tsan_interceptors.cc:900 (pth_pi_busy1.exe+0x00000040c850)
    #1 main <null> (pth_pi_busy1.exe+0x00000048c580)

SUMMARY: ThreadSanitizer: data race (/home/u0920663/CS_4230/src/pthreads/Pi/pth_pi_busy1.exe+0x48c988) in Thread_sum
==================
With n = 1000000 terms,
   Multi-threaded estimate of pi  = 3.141591653589772
   Elapsed time = 5.087534e+00 seconds
   Single-threaded estimate of pi = 3.141591653589774
   Elapsed time = 3.618002e-03 seconds
   Math library estimate of pi    = 3.141592653589793
ThreadSanitizer: reported 2 warnings

Here I have recreated the data race catching performed by using fsanitize. This can allow us to double check
for race conditions which can cripple our program.
Here, the program uses in both sum and factor in code that is run by multiple threads and no distinction
has been given to these memory locations, thus we have no protection from incorrect memory accesses.
