bash-4.2$ gcc-7.3 -o PThreadDPBuggy.exe -fsanitize=address -static-libasan PThreadDPBuggy.c
bash-4.2$ ./PThreadDPBuggy.exe 
Please provide Exp for array sizes and threshold sep by a comma
3,8
Will do DP of 8 sized arrays
Filling arrays now
=================================================================
==21136==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x602000000018 at pc 0x0000004f9fa9 bp 0x7fff38179610 sp 0x7fff38179608
WRITE of size 4 at 0x602000000018 thread T0
    #0 0x4f9fa8 in main (/home/u0920663/CS_4230/src/pthreads/SimpleCBugs/PThreadDPBuggy.exe+0x4f9fa8)
    #1 0x7f4b30c3cc04 in __libc_start_main (/lib64/libc.so.6+0x21c04)
    #2 0x40591b  (/home/u0920663/CS_4230/src/pthreads/SimpleCBugs/PThreadDPBuggy.exe+0x40591b)

0x602000000018 is located 0 bytes to the right of 8-byte region [0x602000000010,0x602000000018)
allocated by thread T0 here:
    #0 0x4be8b0 in __interceptor_malloc ../../.././libsanitizer/asan/asan_malloc_linux.cc:62
    #1 0x4f9efb in main (/home/u0920663/CS_4230/src/pthreads/SimpleCBugs/PThreadDPBuggy.exe+0x4f9efb)
    #2 0x7f4b30c3cc04 in __libc_start_main (/lib64/libc.so.6+0x21c04)

SUMMARY: AddressSanitizer: heap-buffer-overflow (/home/u0920663/CS_4230/src/pthreads/SimpleCBugs/PThreadDPBuggy.exe+0x4f9fa8) in main
Shadow bytes around the buggy address:
  0x0c047fff7fb0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7fc0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7fd0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7fe0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0c047fff8000: fa fa 00[fa]fa fa 00 fa fa fa fa fa fa fa fa fa
  0x0c047fff8010: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8020: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8030: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8040: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8050: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
==21136==ABORTING

As we can see, the address sanitizer detected the overflow on our heap buffer, which can lead to serious problems.
We can use this to make sure we aren't writing outside the bounds we are supposed to write in.
Clearly, somewhere in our code, we are writing something that forces us to fall outside of the allocated memory.
