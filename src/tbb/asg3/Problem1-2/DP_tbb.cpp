#include <iostream>
#include <cmath>
#include <stdlib.h>
#include "tbb/parallel_reduce.h"
#include "tbb/blocked_range.h"
#include "tbb/task_scheduler_init.h"

using namespace std;

// Global arrays where to store the values.
double *A;
double *B;

double parallel_dot_prod(int psize);

int main(int argc, char* argv[]) {
  
  // Grab out the program arguments and parse.
  if (argc < 3) {
    cout << "Usage:\n./DP_tbb <probsize> <numthreads>" << endl;
    exit(EXIT_FAILURE);
  }
  int prob_size = strtol(argv[1], NULL, 10);
  int thread_count = strtol(argv[2], NULL, 10);  

  // Create starting values.
  A = new double[prob_size];
  B = new double[prob_size];
  cout << "Filling arrays with starting values." << endl;
  srand(17); // Seed with 17.
  for (int i=0; i < prob_size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }

  // Set up the number of threads to use.
  tbb::task_scheduler_init init(thread_count);

  // Calculate the final dot product:
  double result = parallel_dot_prod(prob_size);

  cout << "Result is: " << result << endl;

  // Free up memory used.
  delete[] A;
  delete[] B;
}

/*
parallel_dot_prod does the brunt of the work, creating and running the parallel for/reduce construct
using tbb.
 */
double parallel_dot_prod(int psize) {
  return tbb::parallel_reduce(tbb::blocked_range<size_t>(0, psize), 
			      0.f,
			      [](const tbb::blocked_range<size_t> range, double sum) -> double
			      {
				for (int index = range.begin(); index != range.end(); ++index) {
				  double val = A[index] * B[index];
				  sum += val;
				}

				return sum;
			      },
			      [](double x, double y) -> double
			      {
				return x+y;
			      }
			      );
}
