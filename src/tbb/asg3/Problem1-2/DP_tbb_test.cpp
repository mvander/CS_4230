#include <iostream>
#include <cmath>
#include <stdlib.h>
#include "tbb/parallel_reduce.h"
#include "tbb/blocked_range.h"
#include "tbb/task_scheduler_init.h"
#include <timer.h>

using namespace std;

// Global arrays where to store the values.
double *A;
double *B;

double parallel_dot_prod(int psize);

int REPEAT_TEST = 20;

int main(int argc, char* argv[]) {
  
  // Grab out the program arguments and parse.
  if (argc < 3) {
    cout << "Usage:\n./DP_tbb_test.exe <probsize> <numthreads>" << endl;
    exit(EXIT_FAILURE);
  }
  int prob_size = strtol(argv[1], NULL, 10);
  int thread_count = strtol(argv[2], NULL, 10);  

  // Create starting values.
  A = new double[prob_size];
  B = new double[prob_size];
  cout << "Filling arrays with starting values." << endl;
  srand(17); // Seed with 17.
  for (int i=0; i < prob_size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  } 

  for (int threads = 1; threads <= thread_count; ++threads) {
    // Set up the number of threads to use.
    tbb::task_scheduler_init init(threads);

    double start;
    GET_TIME(start);

    for (int test = 0; test < REPEAT_TEST; ++test) {
      // Calculate the final dot product:
      double result = tbb::parallel_reduce(tbb::blocked_range<size_t>(0, prob_size), 
					   0.f,
					   [](const tbb::blocked_range<size_t> range, double sum) -> double
					   {
					     for (int index = range.begin(); index != range.end(); ++index) {
					       double val = A[index] * B[index];
					       sum += val;
					     }

					     return sum;
					   },
					   [](double x, double y) -> double
					   {
					     return x+y;
					   }
					   );

    }

    double finish;
    GET_TIME(finish);
    
    double elapsed = finish - start;

    cout << threads << " & " << elapsed << " \\\\" << endl;
  }

  // Free up memory used.
  delete[] A;
  delete[] B;
}
